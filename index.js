
// Bài 1: Tính Lương
// Input
// - Khai báo biến: Tiền lương 1 ngày + số ngày làm 
// - lấy giá trị từ ô input


// Process
// - ap dung cong thuc tinh luong
  
// Output: Xuất dữ liệu
// - Tổng lương 
function tinhTien(){
    var luongMotNgay = document.getElementById("luong-mot-ngay").value * 1;
    var soNgayLam = document.getElementById("so-ngay-lam").value * 1;
    var tongLuong = (luongMotNgay * soNgayLam);
    document.getElementById("demo1").innerHTML = `${tongLuong} `
};

// Bài 2: Tính Trung Bình

// Input
// - Khai báo biến: 5 số thực được nhập vào
// - Lấy giá trị từ ô input

// Process
// - áp dụng công thức tính trung bình cho 5 số thực

// Output: Xuất dữ liệu
//  - Số Trung Bình
function xuatSo(){
    var num1 = document.getElementById("so-thu-nhat").value*1;
    var num2 = document.getElementById("so-thu-hai").value*1;
    var num3 = document.getElementById("so-thu-ba").value*1;
    var num4 = document.getElementById("so-thu-bon").value*1;
    var num5 = document.getElementById("so-thu-nam").value*1;
    var tinhTrungBinh = (num1 + num2 + num3 + num4 + num5)/5;
    document.getElementById("demo2").innerHTML =`${tinhTrungBinh}`
};

// Bài 3: Quy đổi tiền

// Input
//  - Khai báo biến : tỉ giá mặc định và số tiền nhập vào
//  - Lấy giá trị từ ô input

// Process
//   Áp dụng công thức quy đổi 1USD= 23.500

// Output: Xuất dữ liệu
//   - Số tiền đổi được
function doiTien(){
    var tiGia= 23500;
    var tienNhap = document.getElementById("tien-ngoai").value*1;
    var doiTienValue = tienNhap * tiGia;
    doiTienValue = new Intl.NumberFormat('vn-VN').format(doiTienValue);
    document.getElementById("demo3").innerHTML = `${doiTienValue}`
};

// Bài4: Tính diện tích, chu vi hình chữ nhật

// Input
//  - Khai báo biến: Chiều dài và chiều rộng, diện tích , chu vi
//  - Lấy giá trị từ ô input

// Process
//  - áp dụng công thức tính diện tích và chu vi

// Output: Xuất dữ liệu
//  - Thu được DT và CV

function ketQua(){
    var cD = document.getElementById("chieu-dai").value *1;
    var cR = document.getElementById("chieu-rong").value *1;
    var tinhDienTich = cD * cR;
    var tinhChuVi = (cD + cR) *2;
    document.getElementById("demo4").innerHTML = ` Diện tích: ${tinhDienTich}; Chu vi: ${tinhChuVi}`
};
 
// Bài 5: Tính tổng 2 ký số
// Input
//   - Khai báo biến : Số có 2 chữ số
//   - Lấy giá trị từ ô input

// Process 
//  - áp dụng parseInt để lấy được 2 số 

// Output: Xuất dữ liệu
// - tổng 2 số 
 function tinhTong(){
    var nhapSo = document.getElementById("hai-chu-so").value*1;
    var soThuNhat = parseInt(nhapSo/10);
    var soThuHai = parseInt(nhapSo%10);
    var tongHaiKySo = soThuNhat + soThuHai;
    document.getElementById("demo5").innerHTML=`${tongHaiKySo}`;
};
